# Translation of kcm_kded to Norwegian Bokmål
#
# Knut Yrvin <knut.yrvin@gmail.com>, 2004.
# Axel Bojer <fri_programvare@bojer.no>, 2005.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-10 01:47+0000\n"
"PO-Revision-Date: 2015-02-06 22:25+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr ""

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr ""

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr ""

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr ""

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""

#: ui/main.qml:36
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: ui/main.qml:45
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: ui/main.qml:54
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""

#: ui/main.qml:96
#, kde-format
msgid "All Services"
msgstr ""

#: ui/main.qml:97
#, kde-format
msgctxt "List running services"
msgid "Running"
msgstr ""

#: ui/main.qml:98
#, kde-format
msgctxt "List not running services"
msgid "Not Running"
msgstr ""

#: ui/main.qml:135
#, kde-format
msgid "Startup Services"
msgstr "Oppstartstjenester"

#: ui/main.qml:136
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Når-det-trengs-tjenester"

#: ui/main.qml:153
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: ui/main.qml:220
#, kde-format
msgid "Not running"
msgstr "Kjører ikke"

#: ui/main.qml:221
#, kde-format
msgid "Running"
msgstr "Kjører"

#: ui/main.qml:239
#, kde-format
msgid "Stop Service"
msgstr ""

#: ui/main.qml:239
#, kde-format
msgid "Start Service"
msgstr ""
