# translation of plasma_applet_pager.po to hebrew
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2013.
# tahmar1900 <tahmar1900@gmail.com>, 2008.
# Tahmar1900 <tahmar1900@gmail.com>, 2011.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.pager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-01 02:16+0000\n"
"PO-Revision-Date: 2023-09-30 15:05+0300\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.3.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "כללי"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr "כללי:"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr "הצגת סמלי יישומים במתארי החלונות"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr "הצגת המסך הנוכחי בלבד"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr "ניווט חוזר להתחלה"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "פריסה:"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "ברירת מחדל"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "אופקית"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "אנכית"

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr "תצוגת טקסט:"

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "ללא טקסט"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "מספר פעילות"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "מס׳ שולחן עבודה"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "שם פעילות"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "שם שולחן העבודה"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr "בחירת הפעילות הנוכחית:"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "בחירת שולחן העבודה הווירטואלי הנוכחי:"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "לא עושה כלום"

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr "מציג את שולחן העבודה"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "…וחלון אחד נוסף"
msgstr[1] "…ו־%1 חלונות אחרים"

#: package/contents/ui/main.qml:352
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "חלון אחד:"
msgstr[1] "%1 חלונות:"

#: package/contents/ui/main.qml:364
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "חלון ממוזער:"
msgstr[1] "%1 חלונות ממוזערים:"

#: package/contents/ui/main.qml:439
#, kde-format
msgid "Desktop %1"
msgstr "שולחן עבודה %1"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "מעבר אל %1"

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Show Activity Manager…"
msgstr "הצגת מנהל פעילות…"

#: package/contents/ui/main.qml:591
#, kde-format
msgid "Add Virtual Desktop"
msgstr "הוספת שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:597
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "הסרת שולחן עבודה וירטואלי"

#: package/contents/ui/main.qml:604
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "הגדרות שולחנות עבודה וירטואליים…"

#~ msgid "Icons"
#~ msgstr "סמלים"
